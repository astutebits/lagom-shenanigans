package com.astutebits.lagom;

import akka.NotUsed;
import akka.util.ByteString;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.deser.SerializerFactory;
import java.time.ZonedDateTime;

/**
 * Experiments and workarounds with Lagom.
 */
public interface ShenaniganService extends Service {

  ServiceCall<NotUsed, ByteString> chunked();

  /**
   * Demonstrates how you can hooking into Lagom's default {@link SerializerFactory} to configure
   * the {@link ObjectMapper}. Only useful until Lagom implements an out-of-the-box solution, which
   * is tracked in the linked Github issue.
   *
   * @see <a href="https://github.com/lagom/lagom/issues/682">JSON default serializers for
   * date/datetime differ javadsl to scaladsl</a>
   */
  ServiceCall<NotUsed, ZonedDateTime> time();

  default Descriptor descriptor() {
    return Service.named("test").withCalls(
        Service.namedCall("time", this::time),
        Service.pathCall("/chunked", this::chunked)
    );
  }
}
