package com.astutebits.lagom;

import akka.NotUsed;
import akka.stream.javadsl.Source;
import akka.util.ByteString;
import com.lightbend.lagom.javadsl.server.PlayServiceCall;
import java.util.concurrent.Executor;
import play.libs.streams.Accumulator;
import play.mvc.EssentialAction;
import play.mvc.Results;

public final class ChunkedServiceCall {

  public static PlayServiceCall<NotUsed, ByteString> chunkedCall(final Source<ByteString, ?> source,
      final Executor ec) {
    return wrapCall -> EssentialAction.of(rh ->
        Accumulator.<ByteString>source().map(x -> Results.ok().chunked(source), ec)
    );
  }
}
