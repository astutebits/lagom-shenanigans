package com.astutebits.lagom;

import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;

public final class ShenaniganModule extends AbstractModule implements ServiceGuiceSupport {

  @Override
  protected void configure() {
    bindService(ShenaniganService.class, TheShenaniganService.class);
  }
}
