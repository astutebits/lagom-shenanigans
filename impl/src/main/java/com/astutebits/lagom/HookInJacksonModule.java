package com.astutebits.lagom;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.json.PackageVersion;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.lightbend.lagom.internal.jackson.JacksonObjectMapperProvider;
import com.lightbend.lagom.javadsl.api.deser.SerializerFactory;

/**
 * A Jackson module whose sole purpose is to "hook in" configuration in the default {@link
 * SerializerFactory} used by Lagom. The entire magic happens in {@link
 * JacksonObjectMapperProvider#objectMapper()}, the only thing that's needed is to include this
 * module in the list of modules loaded by Lagom.
 */
public final class HookInJacksonModule extends Module {

  @Override
  public String getModuleName() {
    return "HookInJacksonModule";
  }

  @Override
  public Version version() {
    return PackageVersion.VERSION;
  }

  @Override
  public void setupModule(final SetupContext context) {
    ((ObjectMapper) context.getOwner()).disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

    // Alternatively, a JsonFormat can be specified instead.
//    context.configOverride(ZonedDateTime.class).setFormat(JsonFormat.Value.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
  }
}
