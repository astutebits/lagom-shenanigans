package com.astutebits.lagom;

import akka.NotUsed;
import akka.stream.javadsl.FileIO;
import akka.stream.javadsl.Source;
import akka.util.ByteString;
import com.google.common.io.Resources;
import com.google.inject.Inject;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public final class TheShenaniganService implements ShenaniganService {

  @Inject
  private Executor ec;

  @Override
  public ServiceCall<NotUsed, ZonedDateTime> time() {
    return req -> CompletableFuture.completedFuture(ZonedDateTime.now());
  }

  @Override
  public ServiceCall<NotUsed, ByteString> chunked() {
    try {
      final Source<ByteString, ?> source = FileIO
          .fromPath(Paths.get(Resources.getResource("file.txt").toURI()));
      return ChunkedServiceCall.chunkedCall(source, ec);
    } catch (URISyntaxException e) {
      throw new RuntimeException(e);
    }
  }
}
